#include <iostream>
#include <string>
#include <fstream>
#include "parsing/Lexer.h"
#include "parsing/Parser.h"
#include "core/ModuleManager.h"


using namespace std;

void load(ModuleManager &manager, const string &fileName);

void power(ModuleManager &manager, const string &pwrFile);

void analysis(ModuleManager &manager, const string &moduleId);

void simulation(ModuleManager &manager, const string &mdIdentifier, const string &simulationFile);

void debug(ModuleManager &manager, const string &treeFile);

int main(int argc, char *argv[]) {

    ModuleManager manager;

    cout << "Welcome to SLNS - Simple Logical Circuit Simulator. Please enter a command number:" << endl;
    cout << "load <file> - Load module file" << endl;
    cout << "power <file> - Load power rules file" << endl;
    cout << "simulation <module> <simulation_file> - Perform simulation" << endl;
    cout << "analysis <module> - Perform analysis" << endl;
    cout << "debug <file> - print debug info to file" << endl;
    cout << "quit - Quit" << endl;
    cout << "Enter command: ";
    string command;
    cin >> command;
    cout << endl;

    while (command != "quit") {

        if (command == "load") {

            string file;
            cin >> file;
            load(manager, file);

            for (Module *md : manager.getModules())
                md->update();

        } else if (command == "power") {

            string filename;
            cin >> filename;

            power(manager, filename);

        } else if (command == "analysis") {

            string module;
            cin >> module;

            analysis(manager, module);

        } else if (command == "simulation") {

            string mdIdentifier, simFile;
            cin >> mdIdentifier;
            cin >> simFile;
            simulation(manager, mdIdentifier, simFile);

        } else if (command == "debug") {

            string treeFile;
            cin >> treeFile;

            debug(manager, treeFile);

        } else
            cout << "Unrecognized command." << endl;

        cout << "Enter command: ";
        cin >> command;

    }

    return EXIT_SUCCESS;
}

inline void load(ModuleManager &manager, const string &fileName) {
    manager.loadFromFile(fileName);

    if (manager.wasInterrupted())
        cerr << manager.getLastError() << endl;

}

inline void power(ModuleManager &manager, const string &pwrFile) {
    manager.loadPowerRules(pwrFile);
}

void analysis(ModuleManager &manager, const string &moduleId) {

    Module *md = manager.getModule(moduleId);

    if (md == nullptr)
        cout << "Could not find the selected module.";
    else {

        // Shortest Path

        cout << "  Shortest path: ";
        vector<const ExpressionTree *> shortestPath = md->findShortestPath();
        for (const ExpressionTree *ptr : shortestPath)
            cout << ptr->getLabel() << ' ';
        cout << endl;

        // Longest path

        cout << "  Longest path: ";
        vector<const ExpressionTree *> longestPath = md->findLongestPath();
        for (const ExpressionTree *ptr : longestPath)
            cout << ptr->getLabel() << ' ';
        cout << endl;

        // Logical cones
        cout << "  Required inputs: " << endl;
        for (ModuleOutput *out : md->getOutputs()) {
            cout << "    Output \'" << out->getLabel() << "\': ";
            ExpressionTree exp = out->getExpression();
            for (const ModuleInput *in : exp.getReferencedInputs())
                cout << in->getLabel() << ' ';
            cout << endl;

        }

    }

}

void simulation(ModuleManager &manager, const string &mdIdentifier, const string &simulationFile) {

    Module *md = manager.getModule(mdIdentifier);

    if (md == nullptr)
        cerr << "Could not find the selected module." << endl;
    else {

        ifstream ifStream;
        ifStream.open(simulationFile);

        if (!ifStream) {
            cerr << "Unable to open file " << simulationFile << endl;
            return;
        }

        // Result vector
        vector<string> inputs;
        vector<string> outputs;

        /*
         * We create a vector of all the inputs we have tu update.
         * Inputs which are mapped to an output are skipped.
         * The inputs are in the order in which they are defined in the file.
         */
        vector<ModuleInput *> toUpdate;

        for (Module *mod : md->getDependencies())
            for (ModuleInput *currIn : mod->getInputs())
                if (currIn->getMapping() == nullptr)
                    toUpdate.emplace_back(currIn);

        for (ModuleInput *currIn : md->getInputs())
            if (currIn->getMapping() == nullptr)
                toUpdate.emplace_back(currIn);

        unsigned char line = 1;
        string lineIn;
        while (ifStream >> lineIn) {

            if (lineIn.size() < toUpdate.size()) {
                cout << "Error on line " << line << " - " << toUpdate.size()
                     << " inputs are required to run the simulation." << endl;
                return;
            }

            inputs.emplace_back(lineIn);

            /*
             * Flip flops are updated before updating the inputs so that their output is relative to the previous inputs
             */
            md->tickClock();

            // We update the inputs
            for (char i = 0; i < toUpdate.size(); ++i)
                toUpdate[i]->setValue((bool) (lineIn[i] - '0'));

            // We compute the outputs and save the result
            ostringstream oss;
            for (ModuleOutput *out: md->getOutputs())
                if (!out->isUncertain())
                    oss << out->getValue();
                else
                    oss << 'X';

            // We save the result string so that we can use it in a file/console output
            outputs.emplace_back(oss.str());

            line += 1;

        }

        ifStream.close();

        float consumedPower = md->getConsumedPower();

        /*
         * We want to reset the module in case the user wants to run a second simulation.
         * This clears input and flip flop values. It also resets power consumption.
         */
        md->reset();

        /*
         * Now we need to print to console/file the results
         */
        cout << "Please choose an output mode (file/console): ";
        string outMode;
        cin >> outMode;

        if (outMode == "console") {

            cout << "Format: Inputs -> Outputs" << endl;

            for (char i = 0; i < outputs.size(); ++i)
                cout << inputs[i] << " -> " << outputs[i] << endl;

            if (consumedPower != 0.0)
                cout << "Consumed Power: " << consumedPower << endl;

        } else if (outMode == "file") {

            // Ask for the file
            cout << "Output file: ";
            string outFile;
            cin >> outFile;

            ofstream ofStream;
            ofStream.open(outFile);

            if (!ofStream) {
                cerr << "Could not write to file " << outFile << endl;
                return;
            }

            /*
             * Write results to file
             */
            ofStream << "Format: Inputs -> Outputs" << endl;

            for (char i = 0; i < outputs.size(); ++i)
                ofStream << inputs[i] << " -> " << outputs[i] << endl;

            if (consumedPower != 0.0)
                ofStream << "Consumed Power: " << consumedPower << endl;

            ofStream.close();

            cout << "Wrote results to " << outFile << endl;

        }


    }

}

void debug(ModuleManager &manager, const string &treeFile) {

    ofstream ofStream;
    ofStream.open(treeFile);

    if (!ofStream) {
        cerr << "Could not write to file " << treeFile << endl;
        return;
    }

    for (Module *md : manager.getModules()) {
        ofStream << "################ MODULE: " << md->getIdentifier() << " ################" << endl;

        if (!md->getDependencies().empty()) {
            ofStream << "Dependencies: " << endl;
            for (Module *dep : md->getDependencies())
                ofStream << "  " << dep->getIdentifier() << endl;
        }
        ofStream << endl;

        for (ModuleOutput *out : md->getOutputs()) {
            ofStream << "******** OUT: " << out->getLabel() << " ********" << endl;
            ofStream << out->getExpression() << endl;

            ofStream << "++++++++ Output Paths ++++++++" << endl;
            ExpressionTree exp = out->getExpression();
            vector<vector<const ExpressionTree *>> paths = exp.findPaths(INPUT_NODE);
            int counter = 0;
            for (const vector<const ExpressionTree *> &path : paths) {
                ofStream << "Path " << counter << ": ";
                for (const ExpressionTree *ptr : path)
                    ofStream << ptr->getLabel() << ' ';
                ofStream << endl;
                counter++;
            }
            ofStream << endl;

        }
        ofStream << endl;

        for (FlipFlop *ff : md->getFlipFlops()) {
            ofStream << "******** FF: " << ff->getLabel() << " ********" << endl;
            ofStream << ff->getExpression() << endl;
        }

    }

    ofStream.close();

    cout << "Wrote debug info to " << treeFile << endl;

}

