#include "ModuleOutput.h"

ModuleOutput::ModuleOutput(const string &label) {
    _label = label;
    _expTree = ExpressionTree(this);
}

ModuleOutput::ModuleOutput(const ModuleOutput &other) {
    _label = other._label;
    _expTree = other._expTree;
    _expTree.setData(this);
}

NodeType ModuleOutput::getType() const {
    return OUTPUT_NODE;
}

string ModuleOutput::getLabel() const {
    return _label;
}

bool ModuleOutput::getValue() {
    if (_expTree.getLeftChild() != nullptr)
        return _expTree.getLeftChild()->evaluate();
    else
        return false;
}

bool ModuleOutput::equals(const ExpressionNode *other) const {

    if (other->getType() != OUTPUT_NODE)
        return false;

    const ModuleOutput *toCompare = dynamic_cast<const ModuleOutput *>(other);

    return *toCompare == *this;

}

void ModuleOutput::setExpression(const ExpressionTree &tree) {
    _expTree.setLeftChild(&tree);
}

ExpressionTree &ModuleOutput::getExpression() {
    return _expTree;
}

void ModuleOutput::clearExpression() {
    if (_expTree.getLeftChild() != nullptr)
        _expTree.getLeftChild()->clear();
}

bool ModuleOutput::isUncertain() {
    if (_expTree.getLeftChild() != nullptr)
        return _expTree.getLeftChild()->isUncertain();
    else
        return true;
}

void ModuleOutput::update() {
    if (_expTree.getLeftChild() != nullptr)
        _expTree.getLeftChild()->expandNode();
}

bool ModuleOutput::operator==(const ModuleOutput &other) const {
    return _label == other._label;
}