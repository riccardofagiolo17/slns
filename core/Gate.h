#ifndef SLNS_GATE
#define SLNS_GATE

#include "../expression/ExpressionTree.h"
#include "../parsing/Symbol.h"
#include "PowerData.h"
#include <map>
#include "../util/Utilities.h"

using namespace std;
using namespace SLNS::Parsing;

class Gate : public ExpressionNode {

private:
    SymbolType _operation;
    ExpressionTree _leftOp;
    ExpressionTree _rightOp;

    bool _lastValue;
    bool _lastUncertain;

    float _powerConsumption;

public:

    static map<SymbolType, PowerData> P_DATA;

    explicit Gate(const SymbolType &operation);

    SymbolType getOperation() const;

    void setLeftOperand(const ExpressionTree &leftOp);

    ExpressionTree getLeftOperand() const;

    void setRightOperand(const ExpressionTree &rightOp);

    ExpressionTree getRightOperand() const;

    bool getValue() override;

    bool isUncertain() override;

    bool equals(const ExpressionNode *other) const override;

    void reset();

    float getPowerConsumption() const;

    NodeType getType() const override;

    string getLabel() const override;

    bool operator==(const Gate &other) const;
};


#endif //SLNS_GATE
