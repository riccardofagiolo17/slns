#ifndef SLNS_MODULEOUTPUT
#define SLNS_MODULEOUTPUT

#include <string>
#include "../expression/ExpressionTree.h"

using namespace std;
using namespace SLNS::Expression;

/*
 * This class is used to represent an output
 */
class ModuleOutput : public ExpressionNode {

private:
    string _label;
    ExpressionTree _expTree;

public:
    explicit ModuleOutput(const string &label);

    ModuleOutput(const ModuleOutput &other);

    NodeType getType() const override;

    string getLabel() const override;

    bool getValue() override;

    bool equals(const ExpressionNode *other) const override;

    void setExpression(const ExpressionTree &tree);

    ExpressionTree &getExpression();

    void clearExpression();

    bool isUncertain() override;

    void update();

    bool operator==(const ModuleOutput &other) const;

};


#endif //SLNS_MODULEOUTPUT
