#ifndef SLNS_MODULEINPUT
#define SLNS_MODULEINPUT

#include <string>
#include "../expression/ExpressionNode.h"

using namespace std;
using namespace SLNS::Expression;

class ModuleInput : public ExpressionNode {

private:
    string _label;
    char _value;
    ExpressionNode *_mapping;

public:
    explicit ModuleInput(const string &label);

    NodeType getType() const override;

    string getLabel() const override;

    bool equals(const ExpressionNode *other) const override;

    void setMapping(ExpressionNode *out);

    ExpressionNode *getMapping() const;

    void setValue(const bool &);

    bool getValue() override;

    bool isUncertain() override;

    bool operator==(const ModuleInput &other) const;

    void reset();
};


#endif //SLNS_MODULEINPUT
