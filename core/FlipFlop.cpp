#include "FlipFlop.h"

PowerData FlipFlop::P_DATA = {0, 0};

FlipFlop::FlipFlop(const string &label) {
    _mapping = nullptr;
    _label = label;
    _uncertain = true;
    _value = false;
    _consumedPower = 0.0;
    // We want the root node of the expression to be the current flip flop node
    _expTree = ExpressionTree(this);
}

FlipFlop::FlipFlop(const FlipFlop &other) {
	_mapping = other._mapping;
	_label = other._label;
    _expTree = other._expTree;
    _expTree.setData(this);
	_value = other._value;
	_uncertain = other._uncertain;
	_consumedPower = other._consumedPower;
}

NodeType FlipFlop::getType() const {
    return FLIP_FLOP_NODE;
}

string FlipFlop::getLabel() const {
    return _label;
}

bool FlipFlop::getValue() {
    return _value;
}

bool FlipFlop::isUncertain() {
    return _uncertain;
}

void FlipFlop::update() {
    if (_expTree.getLeftChild() != nullptr)
        _expTree.getLeftChild()->expandNode();
}

bool FlipFlop::equals(const ExpressionNode *other) const {

	if (other->getType() != FLIP_FLOP_NODE)
		return false;

	const FlipFlop *toCompare = dynamic_cast<const FlipFlop *>(other);

	return *toCompare == *this;

}

void FlipFlop::clockTick() {
    bool oldValue = _value;
    bool oldUncertain = _uncertain;

    // If there is no mapping we evaluate the FF Expression Tree
    if (_mapping == nullptr) {

        if (_expTree.getLeftChild() == nullptr)
            return;

        _value = _expTree.getLeftChild()->evaluate();
        _uncertain = _expTree.getLeftChild()->isUncertain();
    }
        // If there's a mapping we evaluate the mapping's expression
    else {
        _value = _mapping->getValue();
        _uncertain = _mapping->isUncertain();
    }

    // We only add power consumption on rise/fall variations from/to certain states
    if (!_uncertain && !oldUncertain) {

        if (oldValue == 0 && _value == 1)
            _consumedPower += P_DATA.rising;

        if (oldValue == 1 && _value == 0)
            _consumedPower += P_DATA.falling;

    }

}

void FlipFlop::setMapping(ExpressionNode *mapping) {
	_mapping = mapping;
}

ExpressionNode *FlipFlop::getMapping() const {
	return _mapping;
}

float FlipFlop::getConsumedPower() const {
	return _consumedPower;
}

void FlipFlop::setExpression(const ExpressionTree &tree) {
    _expTree.setLeftChild(&tree);
}

void FlipFlop::clearExpression() {
    if (_expTree.getLeftChild() != nullptr)
        _expTree.getLeftChild()->clear();
}

ExpressionTree FlipFlop::getExpression() {
    return _expTree;
}

void FlipFlop::reset() {
	_value = false;
	_uncertain = true;
	_consumedPower = 0.0;
}

bool FlipFlop::operator==(const FlipFlop &other) const {
    return _label == other._label;
}