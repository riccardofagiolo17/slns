#ifndef SLNS_MODULEMANAGER
#define SLNS_MODULEMANAGER


#include <list>
#include "Module.h"
#include "../parsing/Lexer.h"
#include "../parsing/Parser.h"
#include <algorithm>

class ModuleManager {

private:
    list<Module *> _modules;

    string _lastError;

public:
    ~ModuleManager();

    ModuleManager() = default;

    list<Module *> getModules();

    Module *getModule(const string &identifier);

    void loadFromStream(istream &is);

    void loadFromFile(const string &fileName);

    void loadPowerRules(const string &fileName);

    bool wasInterrupted() const;

    string getLastError() const;

};


#endif
