#include "ModuleManager.h"

ModuleManager::~ModuleManager() {
    for (Module *const &md: _modules)
        delete md;
}

list<Module *> ModuleManager::getModules() {
    return _modules;
}

Module *ModuleManager::getModule(const string &identifier) {
    for (Module *md : _modules)
        if (md->getIdentifier() == identifier)
            return md;

    return nullptr;
}

void ModuleManager::loadFromStream(istream &is) {

    _lastError.clear();

    Lexer lexer;
    lexer.tokenizeStream(is);

    if (lexer.wasInterrupted()) {
        _lastError = lexer.getLastError();
        return;
    }

    Parser parser(lexer.getTokens());

    parser.loadModules(_modules);

    if (parser.wasInterrupted())
        _lastError = parser.getLastError();

    if (lexer.wasInterrupted())
        _lastError = lexer.getLastError();

}

void ModuleManager::loadFromFile(const string &fileName) {

    _lastError.clear();

    Lexer lexer;
    lexer.tokenizeFile(fileName);

    if (lexer.wasInterrupted()) {
        _lastError = lexer.getLastError();
        return;
    }

    Parser parser(lexer.getTokens());
    parser.loadModules(_modules);

    if (parser.wasInterrupted())
        _lastError = parser.getLastError();

}

void ModuleManager::loadPowerRules(const string &fileName) {

    _lastError.clear();

    Lexer lexer;
    lexer.tokenizeFile(fileName);

    if (lexer.wasInterrupted()) {
        _lastError = lexer.getLastError();
        return;
    }

    Parser parser(lexer.getTokens());
    parser.loadPowerRules();

    if (parser.wasInterrupted())
        _lastError = parser.getLastError();

}

bool ModuleManager::wasInterrupted() const {
    return !_lastError.empty();
}

string ModuleManager::getLastError() const {
    return _lastError;
}