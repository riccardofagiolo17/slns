#include "ModuleInput.h"

ModuleInput::ModuleInput(const string &label) : ExpressionNode() {
    _label = label;
    _value = -1;
    _mapping = nullptr;
}

NodeType ModuleInput::getType() const {
    return INPUT_NODE;
}

string ModuleInput::getLabel() const {
    return _label;
}

bool ModuleInput::equals(const ExpressionNode *other) const {

    if (other->getType() != INPUT_NODE)
        return false;

    const ModuleInput *toCompare = dynamic_cast<const ModuleInput *>(other);

    return *toCompare == *this;

}

void ModuleInput::setMapping(ExpressionNode *out) {
    _mapping = out;
}

ExpressionNode *ModuleInput::getMapping() const {
    return _mapping;
}

void ModuleInput::setValue(const bool &val) {
    _value = val;
}

bool ModuleInput::getValue() {
    if (_mapping == nullptr)
        return _value == -1 ? false : (bool) _value;
    else
        return _mapping->getValue();
}

bool ModuleInput::isUncertain() {
    if (_mapping == nullptr)
        return _value == -1;
    else
        return _mapping->isUncertain();
}

bool ModuleInput::operator==(const ModuleInput &other) const {
    return _label == other._label && _value == other._value;
}

void ModuleInput::reset() {
    //_mapping = nullptr;
    _value = -1;
}