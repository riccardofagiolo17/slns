#include "Module.h"

Module::Module(const string &identifier) {
    _identifier = identifier;
    _clock = false;
}

Module::~Module() {

    /*
     * We need to delete and reset the expressions before deleting inputs, outputs and flip flops.
     * These expressions point to inputs/outputs/flip flops
     */
    for (ModuleOutput *const &out : _outputs)
        out->clearExpression();

    for (FlipFlop *const &ff : _flipFlops)
        ff->clearExpression();

    /*
     * We can now safely delete the rest of the dynamic members
     */
    for (FlipFlop *const &ff : _flipFlops)
        delete ff;

    _flipFlops.clear();

    for (ModuleOutput *const &out : _outputs)
        delete out;

    _outputs.clear();

    for (ModuleInput *const &in : _inputs)
        delete in;

    _inputs.clear();

    for (Gate *const &gate : _gates)
        delete gate;

    _gates.clear();

}

string Module::getIdentifier() const {
    return _identifier;
}

void Module::addDependency(Module *md) {

    if (md->getDependencies().empty())
        _dependencies.emplace_back(md);
    else
        for (Module *dep : md->getDependencies())
            addDependency(dep);

}

vector<Module *> Module::getDependencies() {
    return _dependencies;
}

// Inputs
bool Module::addInput(const ModuleInput &in) {
    if (!isInputDefined(in.getLabel())) {
        _inputs.emplace_back(new ModuleInput(in));
        return true;
    } else
        return false;
}

vector<ModuleInput *> &Module::getInputs() {
    return _inputs;
}

ModuleInput *Module::getInput(const string &label) {
    for (ModuleInput *const &in : _inputs)
        if (in->getLabel() == label)
            return in;
    return nullptr;
}

bool Module::isInputDefined(const string &label) {

    for (ModuleInput *const &in : _inputs)
        if (in->getLabel() == label)
            return true;

    return false;
}

// Outputs

bool Module::addOutput(const ModuleOutput &out) {
    if (!isOutputDefined(out.getLabel())) {
        _outputs.emplace_back(new ModuleOutput(out));
        return true;
    } else
        return false;
}

vector<ModuleOutput *> &Module::getOutputs() {
    return _outputs;
}

ModuleOutput *Module::getOutput(const string &label) {
    for (ModuleOutput *const &out : _outputs)
        if (out->getLabel() == label)
            return out;
    return nullptr;
}

bool Module::isOutputDefined(const string &label) {
    for (auto &out : _outputs)
        if (out->getLabel() == label)
            return true;

    return false;
}

// FlipFlops

bool Module::addFlipFlop(const FlipFlop &flipFlop) {
    if (!isFlipFlopDefined(flipFlop.getLabel())) {
        _flipFlops.emplace_back(new FlipFlop(flipFlop));
        return true;
    } else
        return false;
}

vector<FlipFlop *> &Module::getFlipFlops() {
    return _flipFlops;
}

FlipFlop *Module::getFlipFlop(const string &label) {
    for (FlipFlop *const &flipFlop : _flipFlops)
        if (flipFlop->getLabel() == label)
            return flipFlop;
    return nullptr;
}

bool Module::isFlipFlopDefined(const string &label) {
    for (auto &ff : _flipFlops)
        if (ff->getLabel() == label)
            return true;

    return false;
}

void Module::tickClock() {

    vector<FlipFlop *> toUpdate;

    for (Module *md : _dependencies)
        toUpdate.insert(toUpdate.end(), md->getFlipFlops().begin(), md->getFlipFlops().end());

    for (FlipFlop *const &ff : _flipFlops)
        toUpdate.emplace_back(ff);

    for (FlipFlop *ff : toUpdate)
        ff->clockTick();

}

// Gates

bool Module::addGate(Gate *const &gate) {
    _gates.push_back(gate);
    return true;
}

const vector<Gate *> Module::getGates() {
    return _gates;
}

// Clock

bool Module::hasClock() const {
    return _clock;
}

void Module::setClock(const bool &clk) {
    _clock = clk;
}

// Generic

float Module::getConsumedPower() const {
    float cp = 0;

    for (Module *md : _dependencies)
        cp += md->getConsumedPower();

    for (FlipFlop *const &ff : _flipFlops)
        cp += ff->getConsumedPower();

    for (Gate *const &gate : _gates)
        cp += gate->getPowerConsumption();

    return cp;
}

void Module::update() {

    for (Module *dep : _dependencies)
        dep->update();

    for (ModuleOutput *const &out : _outputs)
        out->update();

    // Expand FF expression
    for (FlipFlop *const &ff : _flipFlops)
        ff->update();

    sort(_flipFlops.begin(), _flipFlops.end(), sortFlipFlop);

}

void Module::reset() {

    for (Module *dep : _dependencies)
        dep->reset();

    for (ModuleInput *const &in : _inputs)
        in->reset();

    for (FlipFlop *const &ff : _flipFlops)
        ff->reset();

    for (Gate *const &gate : _gates)
        gate->reset();

}

vector<const ExpressionTree *> Module::findShortestPath() {
    vector<const ExpressionTree *> shortestPath;

    for (ModuleOutput *out : _outputs) {

        vector<const ExpressionTree *> csPath = out->getExpression().findShortestPath();

        if ((shortestPath.empty() || csPath.size() < shortestPath.size()) && !csPath.empty())
            shortestPath = csPath;

    }

    return shortestPath;

}

vector<const ExpressionTree *> Module::findLongestPath() {
    vector<const ExpressionTree *> longestPath;

    for (ModuleOutput *out : _outputs) {

        vector<const ExpressionTree *> clPath = out->getExpression().findLongestPath();

        if (longestPath.empty() || clPath.size() > longestPath.size())
            longestPath = clPath;

    }

    return longestPath;

}


// Operators

ostream &operator<<(ostream &os, const Module &module) {
    os << "Module: " << endl
       << "  Identifier: " << module.getIdentifier() << endl;

    for (auto &in : module._inputs)
        os << "  INPUT " << in->getLabel() << " V: " << in->getValue() << " U: " << in->isUncertain() << endl;

    for (auto &ff : module._flipFlops)
        os << "  FLIP_FLOP " << ff->getLabel() << " V: " << ff->getValue() << " U: " << ff->isUncertain() << endl;

    for (auto &out : module._outputs)
        os << "  OUTPUT " << out->getLabel() << " V: " << out->getValue() << " U: " << out->isUncertain() << endl;

    os << "  Consumed power: " << module.getConsumedPower() << endl;

    return os;

}

bool Module::operator==(const Module &other) const {
    return _identifier == other._identifier;
}

bool sortFlipFlop(FlipFlop *ff1, FlipFlop *ff2) {

    ExpressionTree tree1 = ff1->getExpression();
    ExpressionTree tree2 = ff2->getExpression();

    return tree1.lookUp(FLIP_FLOP_NODE).size() > tree2.lookUp(FLIP_FLOP_NODE).size();

}
