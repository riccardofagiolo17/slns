#ifndef SLNS_MODULE
#define SLNS_MODULE

#include <string>
#include <vector>
#include <iostream>
#include "Gate.h"
#include "ModuleInput.h"
#include "FlipFlop.h"
#include "ModuleOutput.h"
#include <sstream>
#include <algorithm>

using namespace std;

/*
 * This class represents a logical module
 * The module input order is saved implicitly in the _input vector when the module is loaded.
 * This becomes important once we read inputs from files in simulation mode as each 'column' of the file represents an input and every row represents a clock signal.
 */
class Module {

private:

    string _identifier;
    bool _clock;

    /*
     * All the expression trees loaded by the parser for the current module point to these
     */
    vector<ModuleInput *> _inputs;
    vector<ModuleOutput *> _outputs;
    vector<FlipFlop *> _flipFlops;
    vector<Gate *> _gates;
    vector<Module *> _dependencies;

public:

    explicit Module(const string &identifier);

    ~Module();

    string getIdentifier() const;

    void addDependency(Module *md);

    vector<Module *> getDependencies();

    // Inputs
    bool addInput(const ModuleInput &in);

    vector<ModuleInput *> &getInputs();

    ModuleInput *getInput(const string &label);

    bool isInputDefined(const string &label);

    // Outputs
    bool addOutput(const ModuleOutput &out);

    vector<ModuleOutput *> &getOutputs();

    ModuleOutput *getOutput(const string &label);

    bool isOutputDefined(const string &label);

    // FlipFlops
    bool addFlipFlop(const FlipFlop &flipFlop);

    vector<FlipFlop *> &getFlipFlops();

    FlipFlop *getFlipFlop(const string &label);

    bool isFlipFlopDefined(const string &label);

    void tickClock();

    // Gates
    bool addGate(Gate *const &gate);

    const vector<Gate *> getGates();

    // Clock
    bool hasClock() const;

    void setClock(const bool &clk);

    // Generic
    float getConsumedPower() const;

    void update();

    void reset();

    vector<const ExpressionTree *> findShortestPath();

    vector<const ExpressionTree *> findLongestPath();

    // Operators
    friend ostream &operator<<(ostream &, const Module &module);

    bool operator==(const Module &module) const;

};

bool sortFlipFlop(FlipFlop *ff1, FlipFlop *ff2);

#endif
