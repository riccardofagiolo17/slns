#include "../parsing/Symbol.h"

#ifndef SLNS_POWERDATA
#define SLNS_POWERDATA

using namespace SLNS::Parsing;

typedef struct {
    float rising;
    float falling;
} PowerData;

#endif