#ifndef SLNS_FLIPFLOP
#define SLNS_FLIPFLOP

#include <string>
#include "../expression/ExpressionTree.h"
#include "PowerData.h"

using namespace std;

/*
 * This class is used to represent a Flip Flop
 */
class FlipFlop : public ExpressionNode {

private:
    string _label;
    float _consumedPower;
    ExpressionNode *_mapping;

    /*
     * The expression tree holds pointers to inputs, outputs and flip flops organized in an
     * expression tree that can be evaluated and analyzed
     */
    ExpressionTree _expTree;

    /*
     * These values are updated at every clock tick
     */
    bool _value;
    bool _uncertain;

public:

    /*
     * This is static so that one instance is shared across all FlipFlop objects.
     */
    static PowerData P_DATA;

    explicit FlipFlop(const string &label);

    FlipFlop(const FlipFlop &other);

    NodeType getType() const override;

    string getLabel() const override;

    bool getValue() override;

    bool isUncertain() override;

    void update();

    bool equals(const ExpressionNode *other) const override;

    void clockTick();

    void setMapping(ExpressionNode *mapping);

    ExpressionNode *getMapping() const;

    float getConsumedPower() const;

    void setExpression(const ExpressionTree &tree);

    void clearExpression();

    ExpressionTree getExpression();

    void reset();

    bool operator==(const FlipFlop &other) const;
};


#endif //SLNS_FLIPFLOP
