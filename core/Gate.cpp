#include "Gate.h"

map<SymbolType, PowerData> Gate::P_DATA = {
        {AND,  {0, 0}},
        {OR,   {0, 0}},
        {XOR,  {0, 0}},
        {NAND, {0, 0}},
        {NOR,  {0, 0}},
        {XNOR, {0, 0}},
        {NOT,  {0, 0}}
};


Gate::Gate(const SymbolType &operation) {
    _operation = operation;
    _lastValue = false;
    _lastUncertain = true;
    _powerConsumption = 0;
}

SymbolType Gate::getOperation() const {
    return _operation;
}

void Gate::setLeftOperand(const ExpressionTree &leftOp) {
	_leftOp = leftOp;
}

ExpressionTree Gate::getLeftOperand() const {
	return _leftOp;
}

void Gate::setRightOperand(const ExpressionTree &rightOp) {
	_rightOp = rightOp;
}

ExpressionTree Gate::getRightOperand() const {
	return _rightOp;
}

bool Gate::getValue() {

	bool result;

	switch (_operation) {

	case AND:
		result = _leftOp.evaluate() && _rightOp.evaluate();
		break;

	case OR:
		result = _leftOp.evaluate() || _rightOp.evaluate();
		break;

	case XOR:
		result = _leftOp.evaluate() != _rightOp.evaluate();
		break;

	case NAND:
		result = !(_leftOp.evaluate() && _rightOp.evaluate());
		break;

	case NOR:
		result = !(_leftOp.evaluate() || _rightOp.evaluate());
		break;

	case XNOR:
		result = _leftOp.evaluate() == _rightOp.evaluate();
		break;

	case NOT:
		result = !_leftOp.evaluate();
		break;

	default:
		result = false;
		break;

	}

	if (!_lastUncertain) {

		// Rising
		if (result == 1 && _lastValue == 0)
			_powerConsumption += P_DATA[_operation].rising;

		// Falling
		if (result == 0 && _lastValue == 1)
			_powerConsumption += P_DATA[_operation].falling;

	}

	_lastValue = result;
	_lastUncertain = isUncertain();

	return result;

}

bool Gate::isUncertain() {
    bool uncertain;

    switch (_operation) {

        case NOT:
            uncertain = _leftOp.isUncertain();
            break;

        case NOR:
        case OR:

            uncertain = false;

            if (_leftOp.isUncertain() && _rightOp.isUncertain())
                uncertain = true;

            else if (_leftOp.isUncertain())
                uncertain = !_rightOp.evaluate();

            else if (_rightOp.isUncertain())
                uncertain = !_leftOp.evaluate();

            break;

        case NAND:
        case AND:

            uncertain = false;

            if (_leftOp.isUncertain() && _rightOp.isUncertain())
                uncertain = true;

            else if (_leftOp.isUncertain())
                uncertain = _rightOp.evaluate();

            else if (_rightOp.isUncertain())
                uncertain = _leftOp.evaluate();

            break;

        default:
            uncertain = _leftOp.isUncertain() || _rightOp.isUncertain();
            break;

    }

    return uncertain;

}

bool Gate::equals(const ExpressionNode *other) const {

    if (other->getType() != GATE_NODE)
        return false;

    const Gate *toCompare = dynamic_cast<const Gate *>(other);

    return *toCompare == *this;

}

void Gate::reset() {
	_lastValue = false;
	_lastUncertain = true;
	_powerConsumption = 0;
}

float Gate::getPowerConsumption() const {
	return _powerConsumption;
}

NodeType Gate::getType() const {
	return GATE_NODE;
}

string Gate::getLabel() const {
	return SLNS::Utilities::symbolTypeToString(_operation);
}

bool Gate::operator==(const Gate &other) const {
	return _operation == other._operation;
}
