## SLNS - Simple Logical Network Simulator

This project was created for the exam "Algoritmi e Calcolatori" (Algorithms and Calculators) from the Electronic Engineering bachelor degree @ Polytechnic of Turin.

Everything you see here was coded in 1 month (June 2019) while studying and attending classes for 3 other exams.
It's not particularly good code: many things could be improved (especially the recursive algorithms), some things were wrongly undertood and thus wrongly implemented, but it works well enough.

There are no memory leaks and the program can handle fairly complex sequential circuits.
We had to learn how to create a parser for the specified file formats and how to store data in abstractions so that it could be manipulated later. It was quite a challenge to complete this in one month.

I will improve this project as soon as I have some free time. (Not in the foreseeable future)
