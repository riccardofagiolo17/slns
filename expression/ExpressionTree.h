#ifndef SLNS_EXPRESSIONTREE
#define SLNS_EXPRESSIONTREE

#include "ExpressionNode.h"
#include "../core/ModuleInput.h"
#include <vector>

using namespace SLNS::Expression;
using namespace std;

class ExpressionTree {

private:

    ExpressionTree *_parent;
    ExpressionTree *_left;
    ExpressionTree *_right;
    ExpressionNode *_data;

    bool _isInverted;

    ostream &printWithIndentation(ostream &os, unsigned char level = 0) const;

public:

    ExpressionTree();

    explicit ExpressionTree(ExpressionNode *const &data);

    ~ExpressionTree();

    ExpressionTree(const ExpressionTree &other);

    ExpressionTree &operator=(const ExpressionTree &other);

    ExpressionTree(ExpressionTree &&other) noexcept;

    ExpressionTree &operator=(ExpressionTree &&other) noexcept;
    
	void expandNode();

    void setLeftChild(const ExpressionTree *left);

    ExpressionTree *getLeftChild();

    void setRightChild(const ExpressionTree *right);

    ExpressionTree *getRightChild();

    bool isEmpty() const;

    bool isLeaf() const;

    bool isRoot() const;

    void setData(ExpressionNode *const &data);

    ExpressionNode *getData() const;

    void clear();

    string getLabel() const;

    bool evaluate() const;

    bool isUncertain() const;

    void setInverted(const bool &inverted);

    vector<const ExpressionTree *> lookUp(const NodeType &type) const;

    const ExpressionTree *searchParent(const ExpressionNode *node) const;

    vector<vector<const ExpressionTree *>> findPaths(const NodeType &type) const;

    vector<const ExpressionTree *> findShortestPath() const;

    vector<const ExpressionTree *> findLongestPath() const;

    vector<const ModuleInput *> getReferencedInputs() const;

    friend ostream &operator<<(ostream &os, const ExpressionTree &tree);

};


#endif //SLNS_EXPRESSIONTREE
