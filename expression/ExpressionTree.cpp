#include "ExpressionTree.h"
#include "../core/ModuleOutput.h"
#include "../core/FlipFlop.h"
#include "../core/Gate.h"
#include "../util/Utilities.h"
#include <iostream>
#include <algorithm>

/*
 * Default constructor
 * It's called by the other constructors as well
 */
ExpressionTree::ExpressionTree() {
    _parent = nullptr;
    _left = nullptr;
    _right = nullptr;
    _data = nullptr;
    _isInverted = false;
}

/*
 * Constructor with data initialization for convenience
 */
ExpressionTree::ExpressionTree(ExpressionNode *const &data) : ExpressionTree() {
    setData(data);
}

/*
 * Destructor
 * 1st of the BIG5
 */
ExpressionTree::~ExpressionTree() {
    clear();
}

/*
 * Copy Constructor
 * 2nd of the BIG5
 *
 * The copy constructor is called recursively for the children until a leaf is reached
 *
 * ExpressionTree() is the delegating constructor
 */
ExpressionTree::ExpressionTree(const ExpressionTree &other) : ExpressionTree() {

    _parent = other._parent;

    if (other._left != nullptr && !other._left->isEmpty()) {
        _left = new ExpressionTree(*other._left);
        _left->_parent = this;
    }

    if (other._right != nullptr && !other._right->isEmpty()) {
        _right = new ExpressionTree(*other._right);
        _right->_parent = this;
    }

    if (other._data != nullptr)
        _data = other._data;

    _isInverted = other._isInverted;

}

/*
 * Copy assignment operator
 * 3rd of the BIG5
 */
ExpressionTree &ExpressionTree::operator=(const ExpressionTree &other) {

    _parent = nullptr;
    _left = nullptr;
    _right = nullptr;
    _data = nullptr;

    _parent = other._parent;

    if (other._left != nullptr && !other._left->isEmpty()) {
        _left = new ExpressionTree(*other._left);
        _left->_parent = this;
    }

    if (other._right != nullptr && !other._right->isEmpty()) {
        _right = new ExpressionTree(*other._right);
        _right->_parent = this;
    }

    if (other._data != nullptr)
        _data = other._data;

    _isInverted = other._isInverted;

    return *this;
}

/*
 * Move Constructor
 * Used to steal resources from temporary objects
 * 4th of the BIG5
 *
 * The noexcept specifier is required by stl containers which want to be sure that our move constructor will behave
 */
ExpressionTree::ExpressionTree(ExpressionTree &&other) noexcept {

    _parent = other._parent;

    _left = other._left;

    if (_left != nullptr)
        _left->_parent = this;

    other._left = nullptr;

    _right = other._right;

    if (_right != nullptr)
        _right->_parent = this;

    other._right = nullptr;

    _data = other._data;
    other._data = nullptr;

    _isInverted = other._isInverted;

}

/*
 * Move assignment operator
 * Used to steal resources from temporary objects
 * 5th of the BIG5
 */
ExpressionTree &ExpressionTree::operator=(ExpressionTree &&other) noexcept {

    _parent = other._parent;

    _left = other._left;

    if (_left != nullptr)
        _left->_parent = this;

    other._left = nullptr;

    _right = other._right;

    if (_right != nullptr)
        _right->_parent = this;

    other._right = nullptr;

    _data = other._data;
    other._data = nullptr;

    _isInverted = other._isInverted;

    return *this;

}

/*
 * This function takes a flip flop or output node, which have no children when they are loaded by the parser, and
 * loads their expression as a child.
 */
void ExpressionTree::expandNode() {

    if (_data == nullptr)
        return;

    if (_data->getType() == INPUT_NODE) {
        ModuleInput *in = dynamic_cast<ModuleInput *>(_data);
        // We want to expand the contents of the mapped output on the left branch of the input node
        if (in->getMapping() != nullptr) {
            delete _left;
            _left = new ExpressionTree(dynamic_cast<ModuleOutput *>(in->getMapping())->getExpression());
            _left->_parent = this;
            // We are assuming the output tree was already expanded (via the dependency's update function)
            //_left->expandNode();
        }
    }

    /*
     * _left is where the flip flop expression is saved
     */
    if (_data->getType() == FLIP_FLOP_NODE) {

        FlipFlop *ff = dynamic_cast<FlipFlop *>(_data);

        // If the parent tree has a node just like the current one (feedback) don't expand it.
        if (searchParent(_data) != nullptr)
            return;

        // If the flip flop node already has a saved expression tree expand it
        if (_left != nullptr)
            _left->expandNode();
            // If it doesn't have an expression tree (for example when it's loaded by the parser) initialize it
        else {
            delete _left;

            if (ff->getMapping() == nullptr)
                _left = new ExpressionTree(*ff->getExpression().getLeftChild());
            else
                _left = new ExpressionTree(dynamic_cast<ModuleOutput *>(ff->getMapping())->getExpression());

            _left->_parent = this;
            _left->expandNode();
        }

    }


    /*
     * When we set the expression of a flip flop we set the left branch of the tree
     * We only have to expand that
     */
    if (_data->getType() == OUTPUT_NODE) {

        ModuleOutput *out = dynamic_cast<ModuleOutput *>(_data);

        /* If the output node already has a saved expression tree expand it
         * Output nodes loaded from a correct format always should have a left expression
         */
        if (_left != nullptr)
            _left->expandNode();
        else if (out->getExpression().getLeftChild() != nullptr) {
            delete _left;
            _left = new ExpressionTree(*out->getExpression().getLeftChild());
            _left->_parent = this;
            _left->expandNode();
        }
    }

    if (_data->getType() == GATE_NODE) {

        Gate *gate = dynamic_cast<Gate *>((_data));

        if (!gate->getLeftOperand().isEmpty()) {
            delete _left;
            _left = new ExpressionTree(gate->getLeftOperand());
            _left->_parent = this;
            _left->expandNode();
        }

        if (!gate->getRightOperand().isEmpty()) {
            delete _right;
            _right = new ExpressionTree(gate->getRightOperand());
            _right->_parent = this;
            _right->expandNode();
        }

    }

}

void ExpressionTree::setLeftChild(const ExpressionTree *left) {

    if (left == nullptr)
        return;

    if (left->isEmpty())
        return;

    if (left->_data == nullptr)
        return;

    delete _left;
    _left = new ExpressionTree(*left);
    _left->_parent = this;


}

ExpressionTree *ExpressionTree::getLeftChild() {
    return _left;
}

void ExpressionTree::setRightChild(const ExpressionTree *right) {

    if (right == nullptr)
        return;

    if (right->isEmpty())
        return;

    if (right->_data == nullptr)
        return;

    delete _right;
    _right = new ExpressionTree(*right);
    _right->_parent = this;

}

ExpressionTree *ExpressionTree::getRightChild() {
    return _right;
}

bool ExpressionTree::isEmpty() const {
    return _left == nullptr && _right == nullptr && _data == nullptr;
}

bool ExpressionTree::isLeaf() const {
    return _left == nullptr && _right == nullptr;
}

bool ExpressionTree::isRoot() const {
    return _parent == nullptr;
}

/*
 * Here we set the data
 */
void ExpressionTree::setData(ExpressionNode *const &data) {
    _data = data;
}

ExpressionNode *ExpressionTree::getData() const {
    return _data;
}

/*
 * Clears the current node and deletes all its children
 */
void ExpressionTree::clear() {

    _data = nullptr;

    delete _left;
    _left = nullptr;

    delete _right;
    _right = nullptr;
}

/*
 * This is used to return a label for the current node.
 * It's only used for debugging purposes and may be deleted in a production release
 */
string ExpressionTree::getLabel() const {

    if (_data == nullptr)
        return "UNDEFINED";
    else
        return _data->getLabel();

}

/*
 * This function evaluates the expression tree.
 * Use with discretion as it recursively evaluates the tree (lots of calculations)
 *
 * The tree we are working with is expanded (see the addChild/expandNode functions) but in evaluation we stop at
 * FlipFlop/Output nodes and straight up get their value.
 * Expansion is only used in the look-up algorithms.
 *
 */
bool ExpressionTree::evaluate() const {

    if (_data == nullptr)
        return false;
    else
        return _isInverted == !_data->getValue();

}

/*
 * This function returns true if an input or a flip flop is uncertain (hasn't been initialized)
 */
bool ExpressionTree::isUncertain() const {

    if (_data == nullptr)
        return true;
    else
        return _data->isUncertain();

}

void ExpressionTree::setInverted(const bool &inverted) {
    _isInverted = inverted;
}

/*
 * Look up algorithm that only looks up node types
 */
vector<const ExpressionTree *> ExpressionTree::lookUp(const NodeType &type) const {

    vector<const ExpressionTree *> results;

    if (isEmpty())
        return results;

    // If we're looking for a ModuleInput node and the current one is return it
    if (_data != nullptr && _data->getType() == type) {

        /*
         * We only add inputs to the look-up results if they don't have a mapping
         */
        if (_data->getType() == INPUT_NODE) {
            ModuleInput *in = dynamic_cast<ModuleInput *>(_data);
            if (in->getMapping() == nullptr)
                results.emplace_back(this);
        } else
            results.emplace_back(this);

    }

    if (_left != nullptr) {

        vector<const ExpressionTree *> leftResults = _left->lookUp(type);

        if (!leftResults.empty())
            results.insert(results.end(), leftResults.begin(), leftResults.end());

    }

    if (_right != nullptr) {

        vector<const ExpressionTree *> rightResults = _right->lookUp(type);

        if (!rightResults.empty())
            results.insert(results.end(), rightResults.begin(), rightResults.end());

    }

    return results;

}

/*
 * Look up algorithm that only looks up node types
 */
const ExpressionTree *ExpressionTree::searchParent(const ExpressionNode *node) const {

    if (isRoot())
        return nullptr;

    if (_parent->_data != nullptr && _parent->_data->equals(node))
        return _parent;

    if (!_parent->isRoot()) {
        const ExpressionTree *pResult = _parent->searchParent(node);

        if (pResult != nullptr)
            return pResult;

    }

    return nullptr;

}

/*
 * Returns all paths to a node type
 * If we have 5 inputs it will return paths to all those inputs
 * Useful for the shortest/longest path algorithms
 */
vector<vector<const ExpressionTree *>> ExpressionTree::findPaths(const NodeType &type) const {

    vector<vector<const ExpressionTree *>> paths;

    vector<const ExpressionTree *> found = lookUp(type);

    for (const ExpressionTree *node : found) {

        vector<const ExpressionTree *> pathNodes;

        const ExpressionTree *result = node;

        while (result != nullptr) {
            /*
             * We want everything but output labels in out path vector.
             */
            if (result->_data->getType() != OUTPUT_NODE && result->_data->getType() != INPUT_NODE)
                pathNodes.emplace_back(result);
            else if (result->isRoot() || result->isLeaf())
                pathNodes.emplace_back(result);

            result = result->_parent;

        }

        paths.emplace_back(pathNodes);

    }

    return paths;
}

/*
 * Finds the shortest path in this tree.
 */
vector<const ExpressionTree *> ExpressionTree::findShortestPath() const {

    vector<vector<const ExpressionTree *>> paths = findPaths(INPUT_NODE);

    vector<const ExpressionTree *> shortestPath;

    for (vector<const ExpressionTree *> &currPath : paths)
        if ((currPath.size() < shortestPath.size() || shortestPath.empty()) && !currPath.empty())
            shortestPath = currPath;

    return shortestPath;

}

/*
 * Finds the longest path in this tree.
 */
vector<const ExpressionTree *> ExpressionTree::findLongestPath() const {

    vector<vector<const ExpressionTree *>> paths = findPaths(INPUT_NODE);

    vector<const ExpressionTree *> longestPath;

    for (vector<const ExpressionTree *> &currPath : paths)
        if (currPath.size() > longestPath.size())
            longestPath = currPath;

    return longestPath;

}

/*
 * Finds all the single inputs in this tree.
 */
vector<const ModuleInput *> ExpressionTree::getReferencedInputs() const {

    vector<const ModuleInput *> inputs;

    vector<const ExpressionTree *> inputNodes = lookUp(INPUT_NODE);

    for (const ExpressionTree *inNode : inputNodes) {

        ModuleInput *in = dynamic_cast<ModuleInput *>(inNode->getData());

        if (find(inputs.begin(), inputs.end(), in) == inputs.end())
            inputs.emplace_back(in);

    }

    return inputs;

}

/*
 * Function for printing with indentation. Only used in debugging.
 */
ostream &ExpressionTree::printWithIndentation(ostream &os, unsigned char level) const {
    string indent;

    for (unsigned char i = 0; i < level; ++i)
        indent += "  ";

    os << indent << "This: " << this << endl;

    os << indent << "Parent: ";

    // I don't know why this doesn't work
    if (_parent == nullptr)
        os << "NULL" << endl;
    else
        os << _parent << endl;

    os << indent << "Data: ";
    if (_data != nullptr) {

        switch (_data->getType()) {

            case INPUT_NODE:
                os << "INPUT_NODE " << ((ModuleInput *) _data)->getLabel();
                break;
            case OUTPUT_NODE:
                os << "OUTPUT_NODE " << ((ModuleOutput *) _data)->getLabel();
                break;
            case FLIP_FLOP_NODE:
                os << "FLIP_FLOP_NODE " << ((FlipFlop *) _data)->getLabel();
                break;
            case GATE_NODE:
                os << SLNS::Utilities::symbolTypeToString(((Gate *) _data)->getOperation());
                break;
        }

    } else
        cout << " NULL";

    os << endl;


    os << indent << "Left: ";
    if (_left == nullptr)
        os << "NULL" << endl;
    else {
        os << endl;
        _left->printWithIndentation(os, level + 1);
    }

    os << indent << "Right: ";
    if (_right == nullptr)
        os << "NULL" << endl;
    else {
        os << endl;
        _right->printWithIndentation(os, level + 1);
    }

    return os;
}

ostream &operator<<(ostream &os, const ExpressionTree &tree) {
    os << "ExpressionTree: " << endl;
    tree.printWithIndentation(os);
    return os;
}
