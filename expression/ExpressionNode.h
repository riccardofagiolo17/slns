#ifndef SLNS_EXPRESSIONNODE
#define SLNS_EXPRESSIONNODE

#include <string>

using namespace std;

namespace SLNS {

    namespace Expression {

        typedef enum {
            INPUT_NODE,
            OUTPUT_NODE,
            FLIP_FLOP_NODE,
            GATE_NODE
        } NodeType;

    }

}

/*
 * Superclass for all the expression nodes in our tree.
 */
class ExpressionNode {

public:

    virtual ~ExpressionNode() = default;

    virtual SLNS::Expression::NodeType getType() const = 0;

    virtual bool getValue() = 0;

    virtual bool isUncertain() = 0;

    virtual string getLabel() const = 0;

    virtual bool equals(const ExpressionNode *other) const = 0;

};

#endif //SLNS_EXPRESSIONNODE
