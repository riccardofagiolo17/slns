/*
 * This file contains useful functions which can be used anywhere in the program.
 */

#ifndef SLNS_UTILITIES
#define SLNS_UTILITIES

#include <string>
#include "../parsing/Symbol.h"

using namespace std;
using namespace SLNS::Parsing;

namespace SLNS {

    namespace Utilities {

        // This function is used to convert Symbol Types to strings for logging
        string symbolTypeToString(const SymbolType &t);

    }

}

#endif
