#include "Utilities.h"

string SLNS::Utilities::symbolTypeToString(const SymbolType &t) {

    switch (t) {
        case INVALID:
            return "INVALID";
        case UNDEFINED:
            return "UNDEFINED";
        case IGNORED:
            return "IGNORED";
        case IDENTIFIER:
            return "IDENTIFIER";
        case FF_IDENTIFIER:
            return "FLIP_FLOP_IDENTIFIER";
        case SEPARATOR:
            return "SEPARATOR";
        case NEWLINE:
            return "NEWLINE";
        case NUMBER:
            return "NUMBER";
        case L_RPARENTHESIS:
            return "LEFT_ROUND_PARENTHESIS";
        case R_RPARENTHESIS:
            return "RIGHT_ROUND_PARENTHESIS";
        case L_SPARENTHESIS:
            return "LEFT_SQUARE_PARENTHESIS";
        case R_SPARENTHESIS:
            return "RIGHT_ROUND_PARENTHESIS";
        case MODULE_START:
            return "MODULE_START";
        case MODULE_END:
            return "MODULE_END";
        case INPUT:
            return "INPUT";
        case OUTPUT:
            return "OUTPUT";
        case ASSIGN:
            return "ASSIGN";
        case INSTANCE:
            return "INSTANCE";
        case CLOCK:
            return "CLOCK";
        case ASSIGN_OP:
            return "ASSIGNMENT_OPERATOR";
        case NOT:
            return "NOT_OPERATOR";
        case AND:
            return "AND_OPERATOR";
        case OR:
            return "OR_OPERATOR";
        case NAND:
            return "NAND_OPERATOR";
        case NOR:
            return "NOR_OPERATOR";
        case XOR:
            return "XOR_OPERATOR";
        case XNOR:
            return "XNOR_OPERATOR";
        case SEMICOLON:
            return "SEMICOLON";
        case MAPPED_OUTPUT:
            return "MAPPED_OUT";
    }

    return "{UNKNOWN TOKEN TYPE}";

}