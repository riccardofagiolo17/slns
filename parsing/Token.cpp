#include "Token.h"

#include <iostream>

Token::Token(const SymbolType &type, const string &value) {
    _type = type;
    _value = value;
}

SymbolType Token::getType() const {
    return _type;
}

string Token::getValue() const {
    return _value;
}

ostream &operator<<(ostream &os, const Token &token) {
    return os << token.getValue() << " (" << token.getType() << ")" << endl;
}
