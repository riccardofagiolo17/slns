#include "Lexer.h"

#include <fstream>
#include <algorithm>
#include "../util/Utilities.h"

/*
 * Default constructor because we don't need anything fancy
 */
Lexer::Lexer() = default;

/*
 * Returns the character token if there is one.
 */
SymbolType Lexer::getTokenType(char ch) {

    ch = tolower(ch);

    // Ignored characters
    if (ch == ' ' || ch == '\t')
        return IGNORED;

    // Newline
    if (ch == '\n')
        return NEWLINE;

    // Characters with special behaviour
    if (ch == ';')
        return SEMICOLON;

    // Parenthesis
    if (ch == '(')
        return L_RPARENTHESIS;

    if (ch == ')')
        return R_RPARENTHESIS;

    if (ch == '[')
        return L_SPARENTHESIS;

    if (ch == ']')
        return R_SPARENTHESIS;

    // Operators
    if (ch == '=')
        return ASSIGN_OP;

    // Separators
    if (ch == ',')
        return SEPARATOR;

    return INVALID;
}

/*
 * Returns the string token if there is one
 * Unrecognized strings are treated as identifiers
 */
SymbolType Lexer::getTokenType(string str) {

    transform(str.begin(), str.end(), str.begin(), ::tolower);

    // Number check
    // Doesn't recognize floating point numbers but we aren't going to have them in the file
    bool isNumber = true;

    for (auto &ch : str)
        isNumber &= (isdigit(ch) || ch == '.');

    // Windows style newlines
    if (str == "\r\n")
        return NEWLINE;

    // Keywords
    if (str == "module")
        return MODULE_START;
    else if (str == "endmodule")
        return MODULE_END;
    else if (str == "input")
        return INPUT;
    else if (str == "output")
        return OUTPUT;
    else if (str == "clk")
        return CLOCK;
    else if (str == "instance")
        return INSTANCE;
    else if (str == "assign")
        return ASSIGN;

        // Operators
    else if (str == "not")
        return NOT;
    else if (str == "and")
        return AND;
    else if (str == "or")
        return OR;
    else if (str == "nand")
        return NAND;
    else if (str == "nor")
        return NOR;
    else if (str == "xor")
        return XOR;
    else if (str == "xnor")
        return XNOR;

        // Numbers
    else if (isNumber)
        return NUMBER;

    if (str[0] == 'f' && str[1] == 'f')
        return FF_IDENTIFIER;

    if (str[0] == '.')
        return MAPPED_OUTPUT;

    return IDENTIFIER;
}

/*
 * Reads tokens from an input stream
 */
void Lexer::tokenizeStream(istream &is) {

    char ch;
    string word;

    while (is.get(ch)) {

        if (ch == '#') {

            // Tokenize the word
            if (!word.empty()) {
                _tokens.emplace_back(Token(getTokenType(word), word));
                word.clear();
            }

            // Skip to newline
            while (is.get(ch) && !(ch == '\n' || ch == '\r')) {}
        }

        SymbolType cht = getTokenType(ch);

        if (cht != INVALID) {

            // Tokenize the word
            if (!word.empty()) {
                _tokens.emplace_back(Token(getTokenType(word), word));
                word.clear();
            }

            // After tokenizing the word tokenize the current character
            if (cht != IGNORED)
                _tokens.emplace_back(Token(cht, string(1, ch)));

        } else
            // If the current character is invalid go on reading
            word += ch;

    }

    if (!word.empty()) {
        _tokens.emplace_back(Token(getTokenType(word), word));
        word.clear();
    }

}

void Lexer::tokenizeFile(const string &fileName) {

    ifstream ifStream;
    ifStream.open(fileName);

    if (!ifStream.is_open()) {
        _lastError = "Could not open file " + fileName;
        return;
    }

    tokenizeStream(ifStream);

    ifStream.close();

}

/*
 * @return the tokens this lexer has read
 */
vector<Token> Lexer::getTokens() const {
    return _tokens;
}

bool Lexer::wasInterrupted() const {
    return !_lastError.empty();
}

string Lexer::getLastError() const {
    return _lastError;
}
