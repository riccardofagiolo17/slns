#ifndef SLNS_TOKEN_H
#define SLNS_TOKEN_H

#include <string>
#include "Symbol.h"

using namespace std;
using namespace SLNS::Parsing;

class Token {

private:
    SymbolType _type;
    string _value;

public:

    Token(const SymbolType &type = IDENTIFIER, const string &value = "null_value");

    SymbolType getType() const;

    string getValue() const;

    friend ostream &operator<<(ostream &os, const Token &token);

};


#endif //SLNS_TOKEN_H
