/*
 * This class is a wrapper for parsing functions
 */

#ifndef SLNS_PARSER
#define SLNS_PARSER

#include <vector>
#include <list>
#include <sstream>

#include "Token.h"
#include "../core/Module.h"
#include "../util/Utilities.h"

using namespace SLNS::Parsing;

class Parser {

public:
    Parser(const vector<Token> &tokens);

    void loadModules(list<Module *> &modules);

    void loadPowerRules();

    bool wasInterrupted();

    string getLastError() const;

private:

    // We use the logger to log messages relative to parsing
    unsigned int _line;
    string _errorString;

    vector<Token> _tokens;
    vector<Token>::iterator _tokenIterator;

    /*
     * These are generic functions used to go through the tokens
     */

    bool previousToken();

    Token currentToken() const;

    bool nextToken();

    bool accept(const SymbolType &type);

    bool accept(const vector<SymbolType> &types);

    bool expect(const SymbolType &type);

    bool expect(const vector<SymbolType> &types);

    void skip(const SymbolType &type);

    void skipUntil(const SymbolType &type);


    /*
     * These functions act inside a module definition and are used to load module inputs, outputs and flip flops.
     * All of these end their analysis with the newline character, which might be considered a statement terminator
     */
    void input(Module *&module);

    void output(Module *&module);

    void clock(Module *&module);

    void assignFlipFlop(Module *&module);

    void assignOutput(Module *&module);
    
	void instance(Module *&module, const list<Module *> &modules);

    ExpressionTree expression(Module *&module);

    ExpressionTree readIdentifierNode(Module *&module);

    string readIdentifierLabel();

    string formatErrorString(const string &formatString);

    template<typename T, typename... Types>
    string formatErrorString(string formatString, const T &value, const Types &... args);

    template<typename... Types>
    void error(const string &msg, const Types &... args);

    template<typename... Types>
    string formatErrorString(const string &formatString, const SymbolType &type, const Types &... args);
};

template<typename T, typename... Types>
string Parser::formatErrorString(string formatString, const T &value, const Types &... args) {

    constexpr char t[] = {'?'};

    for (const char &ch : t) {

        size_t start_pos = formatString.find(string(1, ch));

        // The passed value and args... need to have an ostream overload operator for printing.
        ostringstream oss;
        oss << value;

        if (start_pos != string::npos)
            formatString.replace(start_pos, 1, oss.str());


    }

    /*
     * This is the interesting bit.
     * It's a recursive call to this function.
     * The arguments are unpacked and the first argument takes the place of 'value'
     * so the number of args... decreases by one at each call until it reaches 0 and the single-argument function is called.
     *
     * Example: stringFormatter("Hello ? from ? it\'s ?", "Mars", "Earth", "me from the other side")
     * In this case value = "Mars" and args.. = "Earth", "me from the other side" - when the function is called with these arguments this is what happens:
     * 1) value ("Mars") is replaced
     * 2) formatString("Hello Mars from ? it\'s ?", "Earth", "me from the other side") is called
     * 3) value ("Earth") is replaced
     * 4) formatString("Hello Mars from Earth it\'s ?", "me from the other side") is called
     * 5) value ("me from the other side") is replaced
     * 6) formatString("Hello Mars from Earth it\'s me from the other side") is called
     * 7) Function ends
     *
     * Obviously being a template the compiler creates all the different definitions for the functions we need.
     *
     */
    formatString = formatErrorString(formatString, args...);

    return formatString;

}

template<typename... Types>
string
Parser::formatErrorString(const string &formatString, const SLNS::Parsing::SymbolType &type, const Types &... args) {
    return formatErrorString(formatString, SLNS::Utilities::symbolTypeToString(type), args...);
}

template<typename... Types>
void Parser::error(const string &msg, const Types &...args) {
    _errorString = formatErrorString(msg, args...);
}

#endif
