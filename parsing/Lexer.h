#ifndef SLNS_LEXER
#define SLNS_LEXER

#include <string>
#include <vector>
#include "Token.h"

using namespace std;
using namespace SLNS::Parsing;

class Lexer {

private:

    string _lastError;

    vector<Token> _tokens;

    static SymbolType getTokenType(char);

    static SymbolType getTokenType(string);

public:
    Lexer();

    void tokenizeStream(istream &is);

    void tokenizeFile(const string &fileName);

    vector<Token> getTokens() const;

    bool wasInterrupted() const;

    string getLastError() const;
};


#endif
