#include "Parser.h"

#include <algorithm>
#include <sstream>
#include "../util/Utilities.h"
#include "../core/Gate.h"

using namespace SLNS::Utilities;

/*
 * Constructor
 */
Parser::Parser(const vector<Token> &tokens) {
    _line = 1;
    _tokens = tokens;
    _tokenIterator = _tokens.begin();
}

/*
 * Main parser function, all the magic happens here
 * Conceptually hard function
 */
void Parser::loadModules(list<Module *> &modules) {

    /*
     * Here we are using do-while because the token iterator starts at the first token.
     * If it started at the '0' token we could have used a normal while loop but there's no such thing in iterators.
     * We have to run checks on the current token and THEN go on to the next, NOT go on to the next token and THEN run the checks
     */
    do {

        // If a module definition begins
        if (accept(MODULE_START)) {
            nextToken();

            /*
             * Here we are creating a new module in the dynamic memory.
             * A pointer to this object will be passed to the Interpreter which will store a list of all the loaded
             * modules.
             * We are using pointers because when we load logical expressions into a specific module we pass them references
             * to a ModuleInput or a FlipFlop. The Module objects therefore can't be copied as it would break all the references
             * and it wouldn't make much sense anyway.
             */

            expect(IDENTIFIER);
            Module *module = new Module(currentToken().getValue());
            nextToken();

            // Let's skip tokens we are not interested in
            skip(NEWLINE);

            // Expect a left parentheses
            expect(L_RPARENTHESIS);
            nextToken();

            // let's skip straight to the first token inside the parentheses
            skip(NEWLINE);

            /*
             * This while loop operates inside the parentheses.
             * The only accepted statements here are input/output declarations and clock.
             */
            while (!accept(R_RPARENTHESIS) && !wasInterrupted()) {

                // We only accept one of these tokens inside the parentheses
                expect({INPUT, OUTPUT, CLOCK});

                input(module);

                output(module);

                clock(module);

                // Let's skip whatever token might be between this and the next statement
                skip(NEWLINE);

            }

            // Lets skip any allowed characters after the end of the definition
            skip(R_RPARENTHESIS);
            skip(SEMICOLON);
            skip(NEWLINE);

            /*
             * This while loop operates after the definition parentheses close.
             * The accepted statements here are assign, = (assignFlipFlop)
             */
            while (!accept(MODULE_END) && !wasInterrupted()) {

                // We only accept one of these tokens at the beginning of every line
                expect({IDENTIFIER, ASSIGN, FF_IDENTIFIER});

                assignFlipFlop(module);

                assignOutput(module);

                instance(module, modules);

                skip(NEWLINE);

            }

            // If the parser was interrupted of the module was  we won't load the module
            if (!wasInterrupted()) {

                list<Module *>::iterator mdIterator = find(modules.begin(), modules.end(), module);

                if (mdIterator == modules.end())
                    modules.emplace_back(module);
                else {
                    // We delete the previously declared module and substitute the new one
                    Module *toDelete = *mdIterator;
                    modules.remove(toDelete);
                    delete *mdIterator;
                    modules.emplace_back(module);
                }

            }

        }

    } while (nextToken() && !wasInterrupted());

}

void Parser::loadPowerRules() {

    do {

        expect({FF_IDENTIFIER, AND, OR, NOT, XOR, XNOR, NAND, NOR});

        if (accept(FF_IDENTIFIER)) {
            nextToken();

            PowerData ffData;

            expect(SEMICOLON);
            nextToken();

            expect(NUMBER);
            ffData.rising = stof(currentToken().getValue());
            nextToken();

            expect(SEMICOLON);
            nextToken();

            expect(NUMBER);
            ffData.falling = stof(currentToken().getValue());
            nextToken();

            FlipFlop::P_DATA = ffData;

            skipUntil(NEWLINE);

        }

        if (accept({AND, OR, NOT, XOR, XNOR, NAND, NOR})) {
            SymbolType op = currentToken().getType();
            nextToken();

            PowerData opData;

            expect(SEMICOLON);
            nextToken();

            expect(NUMBER);
            opData.rising = stof(currentToken().getValue());
            nextToken();

            expect(SEMICOLON);
            nextToken();

            expect(NUMBER);
            opData.falling = stof(currentToken().getValue());
            nextToken();

            Gate::P_DATA[op] = opData;

            skipUntil(NEWLINE);

        }

    } while (nextToken());

}

bool Parser::wasInterrupted() {
    return !_errorString.empty();
}

string Parser::getLastError() const {
    return _errorString;
}

/*
 * Goes back by one token
 */
bool Parser::previousToken() {

    if ((_tokenIterator - 1) == _tokens.end())
        return false;
    else {
        _tokenIterator -= 1;

        if (currentToken().getType() == NEWLINE)
            --_line;

        return true;
    }

}

/*
 * Wrapper function to ease readability
 */
Token Parser::currentToken() const {
    return *_tokenIterator;
}

/*
 * Continues scanning for the next token
 */
bool Parser::nextToken() {

    if ((_tokenIterator + 1) == _tokens.end())
        return false;
    else {
        _tokenIterator += 1;

        if (currentToken().getType() == NEWLINE)
            ++_line;

        return true;
    }

}

/*
 * Accepts a token type
 */
bool Parser::accept(const SymbolType &type) {
    return currentToken().getType() == type;
}

/*
 * Accepts a vector of token types
 */
bool Parser::accept(const vector<SymbolType> &types) {
    return find(types.begin(), types.end(), currentToken().getType()) != types.end();
}

/*
 * Expects a token type and stops the program if it doesn't find it
 */
bool Parser::expect(const SymbolType &type) {
    if (currentToken().getType() == type)
        return true;
    else
        error("Error: expected ? but got ? - ?", type, currentToken().getType(),
              currentToken().getValue());
    return false;
}

/*
 * Expects a vector of token types and stops the program if it doesn't find it
 */
bool Parser::expect(const vector<SymbolType> &types) {
    if (find(types.begin(), types.end(), currentToken().getType()) != types.end())
        return true;
    else {

        ostringstream oss;

        for (const SymbolType &t : types) {
            oss << symbolTypeToString(t) << ' ';
        }

        error("Error: expecting tokens of type ? but got ? - ?", oss.str(),
              currentToken().getType(), currentToken().getValue());
        return false;
    }

}

/*
 * Skips all instances of a particular SymbolType
 */
void Parser::skip(const SymbolType &type) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    while (accept(type) && nextToken()) {}
}

/*
 * Skips everything until a particular SymbolType
 */
void Parser::skipUntil(const SymbolType &type) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    while (!accept(type) && nextToken()) {}
}


/*
 * Parses input definitions
 */
void Parser::input(Module *&module) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    if (accept(INPUT)) {

        // input directives must be at the beginning of a newline
        previousToken();
        expect(NEWLINE);
        nextToken(); // Back to the identifier

        nextToken();

        // The first identifier doesn't require a comma
        expect(IDENTIFIER);
        string inputLabel;

        // Read INPUT identifiers from the file as long as we're on the same line
        while (!accept(NEWLINE)) {

            if (accept(SEPARATOR) || inputLabel.empty()) {

                if (accept(SEPARATOR)) {
                    nextToken();
                    expect(IDENTIFIER);
                }

                inputLabel = currentToken().getValue();
                nextToken();

                // Array notation support
                if (accept(L_SPARENTHESIS)) {
                    nextToken();

                    expect(NUMBER);
                    unsigned int index = stoi(currentToken().getValue());
                    nextToken();

                    expect(R_SPARENTHESIS);
                    nextToken();

                    for (unsigned int i = 0; i < index; i++) {
                        ostringstream ss;
                        ss << inputLabel << '[' << i << ']';

                        if (!module->addInput(ModuleInput(ss.str()))) {
                            error("Error: duplicate input definition for ?", ss.str());
                            return;
                        }

                    }

                } else {
                    ostringstream ss;
                    ss << inputLabel;

                    if (!module->addInput(ModuleInput(ss.str()))) {
                        error("Error: duplicate input definition for ?", ss.str());
                        return;
                    }
                }

            } else if (!accept(SEPARATOR)) {
                expect(NEWLINE);
            } else
                nextToken();

        }

    }

}

/*
 * Parses output definitions
 */
void Parser::output(Module *&module) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    if (accept(OUTPUT)) {

        // output directives must be at the beginning of a newline
        previousToken();
        expect(NEWLINE);
        nextToken(); // Back to the identifier

        nextToken();

        // The first identifier doesn't require a comma
        expect(IDENTIFIER);
        string outputLabel;

        // Read INPUT identifiers from the file as long as we're on the same line
        while (!accept(NEWLINE)) {

            if (accept(SEPARATOR) || outputLabel.empty()) {

                if (accept(SEPARATOR))
                    nextToken();
                expect(IDENTIFIER);
                outputLabel = currentToken().getValue();
                nextToken();

                // Array notation support
                if (accept(L_SPARENTHESIS)) {
                    nextToken();

                    expect(NUMBER);
                    unsigned int index = stoi(currentToken().getValue());
                    nextToken();

                    expect(R_SPARENTHESIS);
                    nextToken();

                    for (unsigned int i = 0; i < index; i++) {
                        ostringstream ss;
                        ss << outputLabel << '[' << i << ']';

                        if (!module->addOutput(ModuleOutput(ss.str()))) {
                            error("Error: duplicate output definition for ?", ss.str());
                            return;
                        }
                    }

                } else {
                    ostringstream ss;
                    ss << outputLabel;

                    if (!module->addOutput(ModuleOutput(ss.str()))) {
                        error("Error: duplicate output definition for ?", ss.str());
                        return;
                    }
                }

            } else if (!accept(SEPARATOR)) {
                expect(NEWLINE);
            } else
                nextToken();

        }

    }

}

/*
 * Parses clock definitions
 */
void Parser::clock(Module *&module) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    if (accept(CLOCK)) {
        module->setClock(true);
        nextToken();
    }

    expect(NEWLINE);


}

/*
 * Parses flip-flop input definitions
 */
void Parser::assignFlipFlop(Module *&module) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    if (accept(FF_IDENTIFIER)) {

        string identifier = readIdentifierLabel();
        FlipFlop *ffPtr = nullptr;

        ffPtr = module->getFlipFlop(identifier);

        if (ffPtr == nullptr) {
            if (module->hasClock())
                module->addFlipFlop(FlipFlop(identifier));
            else {
                error("Flip flop defined in a module without clock");
                return;
            }
        }

        ffPtr = module->getFlipFlop(identifier);

        expect(ASSIGN_OP);

        nextToken();

        ExpressionTree exp = expression(module);

        // There might have been an error loading the expression
        if (wasInterrupted())
            return;

        ffPtr->setExpression(exp);

    }

}


/*
 * Reads assign directives
 */
void Parser::assignOutput(Module *&module) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    if (accept(ASSIGN)) {

        previousToken();

        if (!accept(NEWLINE)) {
            error("Error: assignment directives must be at the beginning of a new line.");
            return;
        }

        nextToken(); // Back to the assign

        nextToken();

        if (expect(IDENTIFIER)) {

            string identifier = readIdentifierLabel();

            ModuleOutput *outPtr = module->getOutput(identifier);

            if (outPtr == nullptr) {
                error("No output defined for label \'?\'.", identifier);
                return;
            }

            if (accept(ASSIGN_OP)) {
                nextToken();

                ExpressionTree exp = expression(module);

                // There might have been an error loading the expression
                if (wasInterrupted())
                    return;

                outPtr->setExpression(exp);

            }

        }


    }
}

void Parser::instance(Module *&module, const list<Module *> &modules) {

    // All functions called in loadModules should do nothing if the parser was interrupted by an error
    if (wasInterrupted())
        return;

    Module *other = nullptr;

    if (accept(IDENTIFIER)) {

        // Read the other module's identfier
        string otherId = readIdentifierLabel();

        for (Module *md : modules)
            if (md->getIdentifier() == otherId)
                other = md;

        // If the instanced module isn't found...
        if (other == nullptr) {
            error("Error: module ? not found.", otherId);
            return;
        }

        module->setClock(other->hasClock());

        module->addDependency(other);

        expect(INSTANCE);
        nextToken();

        expect(L_RPARENTHESIS);
        nextToken();

        do {
            // Skip ,
            skip(SEPARATOR);

            expect(MAPPED_OUTPUT);

            string id;

            id = readIdentifierLabel();

            ModuleOutput *mappedOut = other->getOutput(id);

            if (mappedOut == nullptr) {
                error("Error: no output ? defined in module ?", id, otherId);
                return;
            }

            expect(ASSIGN_OP);
            nextToken();

            expect({IDENTIFIER, FF_IDENTIFIER});

            if (accept(IDENTIFIER)) {
                id = readIdentifierLabel();
                ModuleInput *mappedIn = module->getInput(id);

                if (mappedIn == nullptr) {
                    error("Error: no input ? defined in module ?", id, module->getIdentifier());
                    return;
                }

                mappedIn->setMapping(mappedOut);

            } else if (accept(FF_IDENTIFIER)) {
                id = readIdentifierLabel();

                FlipFlop *mappedFF = module->getFlipFlop(id);

                if (mappedFF == nullptr) {
                    module->addFlipFlop(FlipFlop(id));
                    mappedFF = module->getFlipFlop(id);
                }

                mappedFF->setMapping(mappedOut);

            }


        } while (currentToken().getType() != R_RPARENTHESIS && nextToken());

        nextToken(); // Skip ) and end on newline
    }

}

ExpressionTree Parser::expression(Module *&module) {

    // Unary Gate
    if (accept({NOT})) {

        Gate *currGate = new Gate(currentToken().getType());
        module->addGate(currGate);
        nextToken();

        ExpressionTree leftOp;

        if (accept({IDENTIFIER, FF_IDENTIFIER})) {

            leftOp = readIdentifierNode(module);

            // There might have been an error loading the identifier node
            if (wasInterrupted())
                return ExpressionTree();

        } else if (accept(L_RPARENTHESIS)) {

            nextToken(); // Skip (

            leftOp = expression(module);

            nextToken(); // Skip )

        }

        currGate->setLeftOperand(leftOp);

        return ExpressionTree(currGate);

    }

        // Binary Gate
    else if (accept({FF_IDENTIFIER, IDENTIFIER, L_RPARENTHESIS})) {

        ExpressionTree leftOp;

        if (accept({IDENTIFIER, FF_IDENTIFIER})) {

            leftOp = readIdentifierNode(module);

            // There might have been an error loading the identifier node
            if (wasInterrupted())
                return ExpressionTree();

            // We check if we only have the left identifier (eg FF1 = x)
            if (accept(NEWLINE))
                return leftOp;

        } else if (accept(L_RPARENTHESIS)) {

            nextToken(); // Skip (

            leftOp = expression(module);

            nextToken(); // Skip )

        }

        expect({AND, OR, XOR, NAND, NOR, XNOR});
        Gate *currGate = new Gate(currentToken().getType());
        module->addGate(currGate);
        nextToken();

        ExpressionTree rightOp;

        if (accept({IDENTIFIER, FF_IDENTIFIER})) {

            rightOp = readIdentifierNode(module);

            // There might have been an error loading the identifier node
            if (wasInterrupted())
                return ExpressionTree();

        } else if (accept(L_RPARENTHESIS)) {

            nextToken(); // Skip (

            rightOp = expression(module);

            nextToken(); // Skip )
        }

        currGate->setLeftOperand(leftOp);
        currGate->setRightOperand(rightOp);

        return ExpressionTree(currGate);

    }

    return ExpressionTree();

}

ExpressionTree Parser::readIdentifierNode(Module *&module) {

    if (accept(IDENTIFIER)) {

        string id = readIdentifierLabel();

        if (module->isInputDefined(id))
            return ExpressionTree(module->getInput(id));
        else if (module->isOutputDefined(id))
            return ExpressionTree(module->getOutput(id));
        else {
            error("Error: Identifier ? has not been declared in this scope.", id);
            return ExpressionTree();
        }

    } else if (accept(FF_IDENTIFIER)) {

        bool isInverted = false;

        string identifier = readIdentifierLabel();

        if (identifier.size() > 4) {
            string end = identifier.substr(identifier.size() - 4);

            if (end == ".NEG") {
                isInverted = true;
                identifier = identifier.substr(0, identifier.size() - 4);
            }
        }

        if (!module->isFlipFlopDefined(identifier)) {

            if (module->hasClock())
                module->addFlipFlop(FlipFlop(identifier));
            else {
                error("Flip flop defined in a module without clock");
                return ExpressionTree();
            }
        }

        ExpressionTree ffTree = ExpressionTree(module->getFlipFlop(identifier));
        ffTree.setInverted(isInverted);

        return ffTree;
    }

    return ExpressionTree();

}

/*
 * We end reading at the first token after the identifier
 */
string Parser::readIdentifierLabel() {

    string idString;

    if (accept(IDENTIFIER)) {
        idString += currentToken().getValue();
        nextToken();

        if (accept(L_SPARENTHESIS)) {
            idString += currentToken().getValue();
            nextToken();
            expect(NUMBER);
            idString += currentToken().getValue();
            nextToken();
            expect(R_SPARENTHESIS);
            idString += currentToken().getValue();
            nextToken();
        }

    } else if (accept(FF_IDENTIFIER)) {
        idString = currentToken().getValue();
        nextToken();
    } else if (accept(MAPPED_OUTPUT)) {
        idString = currentToken().getValue();
        nextToken();

        if (accept(L_SPARENTHESIS)) {
            idString += currentToken().getValue();
            nextToken();
            expect(NUMBER);
            idString += currentToken().getValue();
            nextToken();
            expect(R_SPARENTHESIS);
            idString += currentToken().getValue();
            nextToken();
        }

        idString.erase(0, 1); // Remove the dot
    }


    return idString;

}

string Parser::formatErrorString(const string &formatString) {
    return formatString;
}
