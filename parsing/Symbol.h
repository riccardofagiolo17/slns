#ifndef SLNS_SYMBOL
#define SLNS_SYMBOL

namespace SLNS {

    namespace Parsing {

        typedef enum {

            // Base
            INVALID,
            UNDEFINED,
            SEMICOLON,
            IGNORED,
            IDENTIFIER,
            FF_IDENTIFIER,
            SEPARATOR,
            NEWLINE,
            NUMBER,

            // Parenthesis
                    L_RPARENTHESIS,
            R_RPARENTHESIS,
            L_SPARENTHESIS,
            R_SPARENTHESIS,

            // Keywords
                    MODULE_START,
            MODULE_END,
            INPUT,
            OUTPUT,
            MAPPED_OUTPUT,
            ASSIGN,
            INSTANCE,
            CLOCK,

            // Operators
                    ASSIGN_OP,
            NOT,
            AND,
            OR,
            NAND,
            NOR,
            XOR,
            XNOR

        } SymbolType;

    }

}

#endif